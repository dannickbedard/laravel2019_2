$(document).ready(function () {
    /**
     * Les objets son cacher au début du site ici.
     */
    $('.hide').hide();
    $('#popup-connection').hide();
    $('#popup-enregistrer').hide();
    $('.popup-suppression').hide();
    $('#popup-succes').hide();
    $('.exit').hide();

    $('.crud').click(function () { // Supprime la photo
        console.log('Click');
        $image = $(this).parent().prev();   // on a <img><div class="crud"><img class="supprimer"></div>
        $image.hide();
        $(this).hide();
        $('.imageSurpprimer').hide(); //Cache la photo supprimer
        $('.hide').show(); //Montre le input file
    });

    /**
     * Cache l'image dans le formulaire de modification
     */
    $('form').submit(function () {
        var imagesASupprimer = [];

        // Retrouve l'identifiant de chaque image à supprimer
        $('.imageSurpprimer').each(function () {
            imagesASupprimer.push($(this).attr('data-id'));
        });

        $('<input>').attr({
            type: 'hidden',
            name: 'jsonImagesASupprimer',
            id: 'jsonImagesASupprimer',
            value: JSON.stringify(imagesASupprimer)
        }).appendTo('form');
    });

    /**
     * Affiche le popUp pour ce créer un compte.
     */
    $('.boutonEnregistrer').click(function () {
        $('#popup-enregistrer').show();
        $('.exit').show();
    });

    /**
     * Affiche le popUp pour ce connecter.
     */
    $('.boutonConnection').click(function () {
        $('#popup-enregistrer').hide();
        $('#popup-connection').show();
        $('.exit').show();
    });

    /**
     * Affiche le popUp de confirmation de suppression.
     */
    $('.boutonSuppression').click(function (event) {
        event.preventDefault();
        console.log('click supprimer');
        $(this).next().show();

        $('.exit').show();
        $itemSupprimer = $(this).parent(); // Vas chercher le div de l'item a supprimer.

        /**
         * Supprimer l'item définitivement.
         */
        $('.confirmationSuppression').click(function () {
            console.log('Confirmaiton suppression'); // Indice laisser dans la console
            var $formulaire = $(this).parents("form:first");
            var donneesFormulaire = $formulaire.serialize();
            var actionFormulaire = $formulaire.attr('action');
            $.ajax({
                type: "DELETE",
                url: actionFormulaire,
                dataType: "json",
                data: donneesFormulaire
            }).done(function (response, reussi, textStatus, jqXHR) {
                console.log('done');
                if (response.reussi) { //Vas chercher la variable réussi dans le controller
                    $itemSupprimer.hide(); //Vas chercher la variable plus haut et la cache.
                    $('.popup-suppression').hide(); //Cache le popUp de confirmation de suppression
                    $('.exit').hide(); //Cache le petit X.
                    $('#popup-succes').show(); //Affiche que la suppression ces fait avec succes
                    $('.exit').show(); //Affiche le nouveau X du popup
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 403) {
                    afficherPopupErreur('Vous devez être authentifié et posséder les droits requis pour effectuer cette opération.');
                }
                else {
                    afficherPopupErreur('Nous sommes désolés, il n\'est pas possible de compléter la suppression du produit pour l\'instant.');
                }
            }).always(function (jqXHR, textStatus, errorThrown) {
                console.log('always');
            });
        });
    });

    /**
     * Examen
     *
     * Lorsque la liste réroulante change.
     */
    /*
    |--------------------------------------------------------------------------
    | Examen
    |--------------------------------------------------------------------------
    */
    $("select.status").change(function (event) {
        event.preventDefault();

        var $formulaire = $(this).parents("form:first");     // formulaire dans lequel la balise qui a généré l'appel AJAX était placée
        var donneesFormulaire = $formulaire.serialize();     // donc le jeton anti-CSRF est inclus.
        var actionFormulaire = $formulaire.attr('action');   // l'URL pour l'appel AJAX doit être absolu.

        console.log('action' + actionFormulaire);
        // appel AJAX
        $.ajax({
            type: "POST",
            url: actionFormulaire,
            dataType: "json",
            data: donneesFormulaire
        })
            .done(function (response, textStatus, jqXHR) {
                if (response.valide) {
                    console.log("fonctionne");
                    // l'utilisation de render() a placé dans la variable contenuHTML tout le code HTML de la vue
                    $('#donnees').html(response.contenuHTML);
                } else {
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.log("fail...");
            });
    });

    /**
     * Lors du click sur le X. On cache le popUp.
     */
    $('.exit').click(function () {
        $('#popup-connection').hide();
        $('#popup-enregistrer').hide();
        $('.popup-suppression').hide();
        $('#popup-succes').hide();
        $('.exit').hide();
    });
});