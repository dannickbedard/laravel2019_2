@extends('layouts.app')

@section('metaDescription', 'Liste de skis triés selons leur marque')

@section('titre', 'Liste des skis')
@section('h1Titre', 'Liste de produit')
@section('paragraphe1', "Vous cherchez un répertoire de skis. Vous cherchez de l'informations comportent un ski
        en particulié? Vous le trouverez ici. Nous avons le plus gros répertoire de ski de les marques les plus connues.")
@section('titreCategorieMarque', 'Marque')
{{--Image des catégori--}}
@section('imageMarque')
    <img src="{{asset('medias/armada.jpg')}}" class="img-circle person" alt="Random Name" width="255"
         height="255">
@endsection
@section('imageRecherche')
    <img src="{{asset('medias/rechercheLogo.jpg')}}" class="img-circle person" alt="Random Name" width="255"
         height="255">
@endsection
@section('imageFavoris')
    <img src="{{asset('medias/favorisLogo.jpg')}}" class="img-circle person" alt="Random Name" width="255"
         height="255">
@endsection

@section('titreCategorieRecherche', 'Recherche')
@section('titreCategorieFavori', 'Favori')
{{--Section pour truc plus complquer --}}
@section('contenu')
    <div class="container text-center">
        <h1>Tout les items</h1>
        <br>
        @foreach($marques as $marque)
            <h4>{{ $marque->nom}}</h4>
            @foreach($marque->produit as $produits)

                <p>{{ $produits->nom }}</p>
            @endforeach
        @endforeach
    </div>
@endsection

