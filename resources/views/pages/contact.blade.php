@extends('layouts.app')

@section('metaDescription')
    @if($textes->first()->count())
        {!! $textes->first()->description !!}
    @else
        Oups... Il ces passé un problème. Veuillez réessayer plus tard.
    @endif
@endsection
@section('titre')
    @if($textes->first()->count())
        {!! $textes->first()->title !!}
    @else
        Oups... Il ces passé un problème. Veuillez réessayer plus tard.
    @endif
@endsection
<p>@include('flash::message')</p>

@section('afficherCommentaires')
    <div class="container text-center">
        <div class="container">
            <h2>Qui sommes nous?</h2>
            <p>
                @if($textes->first()->count())
                    {!!$textes->first()->texte!!}
                @else
                    Oups... Il ces passé un problème. Veuillez réessayer plus tard
                @endif</p>
            <h2>Comment me rejoindre.</h2>
            <ul>
                <li>bedard.dannick@carrefour.cegepvicto.ca</li>
                <li>789-456-3210</li>
                <li>475 Rue Notre-Dame Est, Victoriaville, QC G6P 4B3</li>
            </ul>
        </div>
        @auth
            @can('showPage', App\Page::class)
                <a class="btn-primary btn" href="{{route('page.edit', ['url' => Request::path()])}}">Édtion page</a>
            @endcan
        @endauth
        <br>
        @include('layouts.enregistrerCommentaire')
    </div>
@endsection

