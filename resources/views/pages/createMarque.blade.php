@extends('layouts.app')
@section('metaDescription', 'Ajouter une marque à ce merveilleux répertoire de ski. ')

@section('titre', 'Ajouter une marques')
@include('flash::message')
@section('contenu')
    <div class="container text-center">
        <h1>Ajouter une marque</h1>
        <br>
        <form method="post" action="{{ route('marques.store') }}">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="nom" class="col-form-label col-sm-2">*Nom : </label>
                <div class="col-sm-10">
                    <input maxlength="150 required" type="text" class="form-control" name="nom" id="nom">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-form-label col-sm-2">*Description : </label>
                <div class="col-sm-10">
                    <input type="text" required class="form-control" name="description" id="description">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <input type="submit" class="btn btn-primary" value="Enregistrer">
                </div>
            </div>
        </form>
    </div>
@endsection
@if(isset($errors) && $errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

{{--Source : https://github.com/proengsoft/laravel-jsvalidation--}}

<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\ProduitRequest') !!}