
{{--/*
|--------------------------------------------------------------------------
| Examen liste clients trier par une liste déroulante
|--------------------------------------------------------------------------
*/--}}
{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Liste de client par statues')

@section('titre', 'Liste de clients')
@section('h1Titre', 'Liste de clients')
<p>@include('flash::message')</p>
@section('paragraphe1', "Choisir le statue pour voir la liste des clients qui sauront affichée dans ce statue")
@section('contenu')
    <div class="container text-center">
        <form method="post" action="{{ route('client.get') }}">
            @csrf
            <div class="form-group row">
                <label for="status" class="col-form-label col-sm-2">Statut </label>
                <select class="form-control col-sm-10 status" id="statut_id" name="statut_id">
                    <option value="">Veuillez choisir...</option>
                    @foreach($status as $statu)
                        <option value="{{$statu->id}}">{{$statu->description}}</option>
                    @endforeach
                </select>
            </div>
        </form>
        <div id="donnees">
        </div>
    </div>
@endsection


