{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription')
    @if($textes->first()->count())
        {!! $textes->first()->description !!}
    @else
        Oups... Il ces passé un problème. Veuillez réessayer plus tard.
    @endif
@endsection
@section('Accueil' , 'active')
@section('titre')
    @if($textes->first()->count())
        {!! $textes->first()->title !!}
    @else
        Oups... Il ces passé un problème. Veuillez réessayer plus tard.
    @endif
@endsection
@section('h1Titre')
    @if($textes->first()->count())
        {!! $textes->first()->titre !!}
    @else
        Oups... Il ces passé un problème. Veuillez réessayer plus tard.
    @endif
@endsection
<p>@include('flash::message')</p>
@section('paragraphe1' )
    <p>@if($textes->first()->count())
            {!!$textes->first()->texte!!}
        @else
            Oups... Il ces passé un problème. Veuillez réessayer plus tard.
        @endif</p>
@endsection
@section('contenu')
    <div class="container text-center">
        <div class="row text-cente">
            <div class="">
                <p class="text-center"><strong>Marques</strong></p><br>
                <a href="{{action('ItemsController@index')}}">
                    <img src="{{asset('medias/armada.jpg')}}" class="img-circle person imageCategori"
                         alt="Iamge de marque de ski">
                </a>
            </div>
        </div>
        @can('showPage', App\Page::class)
            {{-- Ici nous fesont un exeption au Request::path()... car cela nous retourne rien, car la route path n'est
            rien vue que ces la page d'accueil --}}
            <a class="text-center btn btn-primary" href="{{route('page.edit', ['url' =>'accueil'])}}"> édtion
                page</a>
        @endcan
    </div>
@endsection
@section('afficherCommentaires')
    @include('layouts.enregistrerCommentaire')
@endsection