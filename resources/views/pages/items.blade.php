@extends('layouts.app')
@section('metaDescription', 'Liste de ski qui ont été ajoutée au répertoire.')

@section('Item' , 'active')
@section('titre', 'Liste des produits')
@section('h1Titre', 'Liste de produits')

@section('paragraphe1', "Vous cherchez un répertoire de skis. Vous cherchez l'information comportent un ski
        en particulier? Vous le trouverez ici. Nous avons le plus gros répertoire de ski de les marques les plus connues.")
@section('titreCategorieMarque', 'Marque')
@section('titreCategorieRecherche', 'Recherche')
@section('titreCategorieFavori', 'Favori')
{{--Section pour truc plus complquer --}}
@section('contenu')
    <div class="container text-center">
        <h1>Tout les items</h1>
        <p>@include('flash::message')</p>
        <br>
        @foreach($produits as $produit)
            <div class="">
                <h4>{{ $produit->nom}}</h4>
                @if($produit->imageProduits->count())
                    <img class="imageListe" alt="{{$produit->description}}"
                         src="{{asset('medias/produits/' .  $produit->imageProduits->first()->image)}}">
                @elseif($produit->imageProduits->count() > 1)
                    <img class="imageListe" alt="{{$produit->description}}"
                         src="{{asset('medias/produits/' .  $produit->imageProduits->first()->image)}}">
                @endif
                @auth
                    @can('edit', $produit)
                        <a data-target="#" href="{{ route('items.edit', [$produit->id]) }}"><img height="25" width="25"
                                                                                                 src="{{asset('medias/commun/edit.svg')}}"></a>
                        {{--La ou je déclanche le popUp--}}
                        <img height="25" width="25" class="boutonSuppression"
                             src="{{asset('medias/commun/cancel-button.svg')}}">
                    @endcan
                @endauth
                {{--PopUp Confirmation--}}
                <div id="{{$produit->id}}" class="popup popup-suppression ">
                    <span class="helper"></span>
                    <div class="">
                        <a class="exit">X</a>
                        <div class="">
                            <br>
                            <form class="supprimer" method="POST"
                                  action="{{ route('produits.destroy', [ $produit ]) }}">
                                @csrf
                                @method('DELETE')
                                <h1>{{$produit->nom}}</h1>
                                <h3>Voulez vous vraiment supprimer ce produit?</h3>
                                <p>La suppression est une opération irréversible. Il n'a aucun façons de
                                    revenir en
                                    arrière.</p>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <span data-target="#" class="btn btn-primary confirmationSuppression">Confirmation
                                            supprimer</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{--PopUp Succes--}}
    <div class="container text-center">
        <div id="popup-succes" class="popup">
            <span class="helper"></span>
            <div class="">
                <a class="exit">X</a>
                <div class="">
                    <br>
                    <p>La suppression à fonctionner avec succes!</p>
                </div>
            </div>
        </div>
    </div>
@endsection

