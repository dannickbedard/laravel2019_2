@extends('layouts.app')
@section('metaDescription', 'Liste de marques ajoutés aux répertoire.')
@section('Marque' , 'active')
@section('titre', 'Liste des marques')
@section('h1Titre', 'Liste des marques')

@section('paragraphe1', "Vous chercher un répertoire des skis que une marque a dejat fait. Vous chercher l'information comportent un ski
        en particulier? Vous le trouverez ici. Nous avons le plus gros répertoire de ski de les marques les plus connues.")
@section('titreCategorieMarque', 'Marque')

@section('titreCategorieRecherche', 'Recherche')
@section('titreCategorieFavori', 'Favori')
{{--Section pour truc plus complquer --}}
@section('contenu')
    <div class="container text-center">
        <h1>TOUT LES MARQUE</h1>
        @foreach($marques as $marque)
            <h4>{{ $marque->nom}}</h4>
            @if($marque->imageMarques->count())
                <img class="imageListe" alt="{{$marque->description}}"
                     src="{{asset('medias/marques/' .  $marque->imageMarques->first()->image)}}">
            @elseif($marque->imageMarques->count() > 1)
                <img class="imageListe" alt="{{$marque->description}}"
                     src="{{asset('medias/marques/' .  $marque->imageMarques->first()->image)}}">
            @endif
        @endforeach
    </div>
@endsection

