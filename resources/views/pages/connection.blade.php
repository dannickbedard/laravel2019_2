{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Créer votre compte ici. Avoir la possibiliter de créer une liste de favori')

@section('ajouteClient' , 'active')
@section('titre', 'Créer un compte')
<p>@include('flash::message')</p>
@section('contenu')
    <div class="enregistrerClient">
        @include('layouts.connectionUsager')
    </div>
@endsection