@extends('layouts.app')
@section('metaDescription', 'Ajouter un produit a ce merveilleux répertoire de ski.')
@section('titre', 'Ajouter un produit')
@include('flash::message')
@section('contenu')
    <div class="container text-center">
        <h1>Ajouter un produit de ski</h1>
        <br>
        <form method="post" action="{{ route('produits.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="nom" class="col-form-label col-sm-2">*Nom : </label>
                <div class="col-sm-10">
                    <input type="text" maxlength="150" required class="form-control" name="nom" id="nom"
                           value="{{ old('nom') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-form-label col-sm-2">*Description : </label>
                <div class="col-sm-10">
                    <input type="text" required class="form-control" name="description" id="description"
                           value="{{ old('description') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-form-label col-sm-2">*Marque : </label>
                <select class="form-control" required name="marque_id[]">
                    <option value="" selected>Veuillez choisir une marques</option>
                    @foreach($marques as $marque)
                        <option value="{{ $marque->id }}">{{ $marque->nom }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group row">
                <label for="image" class="col-form-label col-sm-2">* Photo : </label>
                <div class="col-sm-10">
                    <input required id="image" name="image" type="file">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <input type="submit" class="btn btn-primary" value="Enregistrer">
                </div>
            </div>
        </form>
    </div>
@endsection
@if(isset($errors) && $errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
{{--Source : https://github.com/proengsoft/laravel-jsvalidation--}}
<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\ProduitRequest') !!}


