<!DOCTYPE html>
<html lang="fr-ca">
<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>@yield('titre')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content='@yield('metaDescription')'/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
          href="{{asset('medias/favicon.jpg')}}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/messtyle.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://craftpip.github.io/jquery-confirm/"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=t8kb1vs6gweqxvgjsz0c6zi0a5mh1upu824kr6884i7md8m3"></script>

    <script src="{{asset('js/script.js')}}"></script>
    <script src="{{asset('js/popup.js')}}"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item ">
                <a class="nav-link {!! Request::is('/') ? 'active' : '' !!}" href="/">Accueil <span class="sr-only">(current)</span></a>
            </li>
            @unless (App::isDownForMaintenance())
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        AFFICHER
                    </a>
                    <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item bg-dark {!! Request::is('items') ? 'active' : '' !!}" href="{{ url('items')}}">Item</a>
                        <a class="dropdown-item bg-dark {!! Request::is('marques') ? 'active' : '' !!}" href="{{ url('marques')}}">Marque</a>
                        <a class="dropdown-item bg-dark {!! Request::is('marquesProduits') ? 'active' : '' !!}" href="{{ url('marquesProduits')}}">Marque et item</a>
                    </div>
                </li>
                @auth
                    @can('create', App\Produit::class)
                        <li class="nav-item dropdown ">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                AJOUTER
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item bg-dark {!! Request::is('produitscreation') ? 'active' : '' !!}" href="{{ url('produitscreation')}}">Ajouter produit</a>
                                <a class="dropdown-item bg-dark {!! Request::is('marquescreation') ? 'active' : '' !!}" href="{{ url('marquescreation')}}">Ajouter marque</a>
                            </div>
                        </li>
                    @endcan
                @endauth
                <li class="nav-item">
                    <a class="nav-link {!! Request::is('contact') ? 'active' :  '' !!}"
                       href="{{ url('contact')}}">Contact</a>
                </li>
        </ul>
        @auth
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('logout')}}">Déconnexion</a>
                </li>
            </ul>
        @else
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link boutonEnregistrer  {{ Request::is('clientCreation') ? 'active' : '' }}">Inscrivez-vous</a>
                </li>

            </ul>
        @endauth
        @endunless
        <p class="navbar-nav float-right">Site web étudiant</p>
    </div>
</nav>

<div id="popup-enregistrer" class="popup">
    <span class="helper"></span>
    <div class="">
        <a class="exit">X</a>
        <div class="">
            <h1>Créer votre compte</h1>
            <br>
            <form method="post" action="{{ route('usagers.store') }}">
                {{ csrf_field() }}
                <div class="form-group row ">
                    <label for="nom" class="col-form-label ">* Nom : </label>
                    <div class="col-sm-12">

                        <input maxlength="150" value="{{ old('nom') }}" type="text" class="form-control" name="nom"
                               id="nom"
                               placeholder="Entrer votre nom">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prenom" class="col-form-label ">* Prenom : </label>
                    <div class="col-sm-12">
                        <input type="text" maxlength="150" value="{{ old('prenom') }}" class="form-control"
                               name="prenom"
                               id="prenom" placeholder="Entrer votre prenom">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="courriel" class="col-form-label ">*Courriel : </label>
                    <div class="col-sm-12">
                        <input type="text" value="{{ old('courriel') }}" required class="form-control" name="courriel"
                               id="courrielEnregistrer" placeholder="Entrer votre courriel">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="motdepasses" class="col-form-label ">*Mot de passe : </label>
                    <div class="col-sm-12">
                        <input type="password" value="" required class="form-control" name="motdepasses"
                               id="motdepassesEnregistrer" placeholder="Entrer votre mot de passe"></div>
                </div>
                <div class="form-group row ">
                    <label for="motdepasse_confirmation" class="col-form-label ">*Confirmation mot de passe : </label>
                    <div class="col-sm-12">
                        <input type="password" value="" required class="form-control" name="motdepasse_confirmation"
                               id="motdepasse_confirmation" placeholder="Confirmer votre mot de passe"></div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12"><a class="boutonConnection">Vous avez un compte?</a></div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Enregistrer">
                    </div>
                </div>
            </form>
        </div>
        @if(isset($errors) && $errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
    </div>
</div>

<div id="popup-connection" class="popup">
    <span class="helper"></span>
    <div class="">
        <a class="exit">X</a>
        <div class="">
            <h1>Connexion</h1>
            <br>
            <form method="post" action="{{ route('usager.login') }}">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="courriel" class="col-form-label">*Courriel : </label>
                    <div class="col-sm-12">
                        <input type="text" value="{{ old('courriel') }}" required class="form-control" name="courriel"
                               id="courrielConnexion" placeholder="Entrer votre courriel">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="motdepasses" class="col-form-label ">*Mot de passe : </label>
                    <div class="col-sm-12">
                        <input type="password" value="" required class="form-control" name="motdepasses"
                               id="motdepassesConnexion" placeholder="Entrer votre mot de passe"></div>
                </div>
                <div class="form-group row">
                    <div class=""></div>
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Connexion">
                    </div>
                </div>
            </form>
        </div>
        @if(isset($errors) && $errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
    </div>
</div>

<div id="" class="container text-center">
    <h3>@yield('h1Titre')</h3>
    <p><em>@yield('h1TitreSousTitre')</em></p>
    <p>@yield('paragraphe1')</p>
    <br>
</div>
@yield('contenu')
<br>
<!-- Container (TOUR Section) -->
@yield('section2')
<!-- Container (Contact Section) -->
@yield('contacter')
@yield('afficherCommentaires')

{{--/*
|--------------------------------------------------------------------------
| Examen bouton ajouter
|--------------------------------------------------------------------------
*/--}}
@auth
    <div class="container text-center">
        <a class="btn btn-dark" href="{{route('client.show')}}">Afficher clients</a>
    </div>
@endauth
<footer class="text-center">

    <div class="container">

        <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
            <span class="glyphicon glyphicon-chevron-up"></span>
        </a><br><br>
        <p>Dannick Bedard <?php echo date("Y")?></p>
        <p>Ce site est un projet étudiant réalisé dans le cadre du cours Développement Web 2 au cégep de
            Victoriaville.
            Toutes les données présentées sont fictives.</p>
        <p>Bootstrap Theme Made By <a href="https://www.w3schools.com" data-toggle="tooltip"
                                      title="Visit w3schools">www.w3schools.com</a>
        </p>
        <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from
            <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a
                    href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC
                3.0 BY</a></div>
    </div>
</footer>

<script>
    $(document).ready(function () {
        // Initialize Tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {

                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    })
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137829012-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-137829012-1');
</script>
<script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
@yield('js')
</body>
</html>



