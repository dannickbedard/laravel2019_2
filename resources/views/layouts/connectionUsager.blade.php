<div class="container">
    <h1>Connexion</h1>
    <br>
    <form method="post" action="{{ route('usager.login') }}">
        {{ csrf_field() }}
        <div class="form-group row">
            <label for="courriel" class="col-form-label col-sm-2">*Courriel : </label>
            <div class="col-sm-10">
                <input type="text" value="{{ old('courriel') }}" required class="form-control" name="courriel"
                       id="courriel" placeholder="Entrer votre courriel">
            </div>
        </div>
        <div class="form-group row">
            <label for="motdepasses" class="col-form-label col-sm-2">*Mot de passe : </label>
            <div class="col-sm-10">
                <input type="password" value="" required class="form-control" name="motdepasses"
                       id="motdepasses" placeholder="Entrer votre mot de passe"></div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <input type="submit" class="btn btn-primary" value="Connexion">
            </div>
        </div>
    </form>
</div>

@if(isset($errors) && $errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
{{--<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>--}}
{{--{!! JsValidator::formRequest('App\Http\Requests\UsagerRequest') !!}--}}