<div class="container">
    <h1>Créer un votre compte</h1>
    <br>
    <form method="post" action="{{ route('usagers.store') }}">
        {{ csrf_field() }}
        <div class="form-group row">
            <label for="nom" class="col-form-label col-sm-2">*Nom : </label>
            <div class="col-sm-10">
                <input maxlength="150" value="{{ old('nom') }}" type="text" class="form-control" name="nom" id="nom"
                       placeholder="Entrer votre nom">
            </div>
        </div>
        <div class="form-group row">
            <label for="prenom" class="col-form-label col-sm-2">*prenom : </label>
            <div class="col-sm-10">
                <input type="text" maxlength="150" value="{{ old('prenom') }}" class="form-control" name="prenom"
                       id="prenom" placeholder="Entrer votre prenom">
            </div>
        </div>
        <div class="form-group row">
            <label for="courriel" class="col-form-label col-sm-2">*Courriel : </label>
            <div class="col-sm-10">
                <input type="text" value="{{ old('courriel') }}" required class="form-control" name="courriel"
                       id="courriel" placeholder="Entrer votre courriel">
            </div>
        </div>
        <div class="form-group row">
            <label for="motdepasses" class="col-form-label col-sm-2">*Mot de passe : </label>
            <div class="col-sm-10">
                <input type="password" value="" required class="form-control" name="motdepasses"
                       id="motdepasses" placeholder="Entrer votre mot de passe"></div>
        </div>
        <div class="form-group row">
            <label for="motdepasse_confirmation" class="col-form-label col-sm-2">*Confirmation mot de passe : </label>
            <div class="col-sm-10">
                <input type="password" value="" required class="form-control" name="motdepasse_confirmation"
                       id="motdepasse_confirmation" placeholder="Confirmer votre mot de passe"></div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <input type="submit" class="btn btn-primary" value="Enregistrer">
            </div>
        </div>
    </form>
</div>

@if(isset($errors) && $errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

@endif
{{--Source : https://github.com/proengsoft/laravel-jsvalidation--}}
<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\UsagerRequest') !!}


