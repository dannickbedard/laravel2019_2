{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Oups une erreur ces produite')
@section('titre', 'Oups une erreur est survenue')
@section('h1Titre', 'Oups une erreur est survenue')
<p>@include('flash::message')</p>

@section('contenu')
    <div class="container">
        <p>Nous sommes désolé, mais cette page n'est pas encore disponible pour le moment. Continué à naviguer sur
            notre site! Nous avons encore beaucoup plus de choses à découvrires</p>
    </div>
    <div class="list-group">
        <a href="{{url('/')}}" class="list-group-item list-group-item-action">Accueil</a>
        <a href="{{url('items')}}" class="list-group-item list-group-item-action">Item</a>
        <a href="{{url('marque')}}" class="list-group-item list-group-item-action">Marque</a>
        <a href="{{url('marqueProduits')}}" class="list-group-item list-group-item-action">Marque et produit</a>
    </div>
@endsection