{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Oups une erreur ces produite')

@section('titre', 'Oups une erreur est survenue')
@section('h1Titre', 'Oups d\'éxécution est survenue')
<p>@include('flash::message')</p>

@section('contenu')
    <div class="container">
        <p>Nous sommes désolé, il est surenue un problème lors de l'éxécution du programme.</p>
    </div>
    <div class="list-group">
        <a href="{{url('/')}}" class="list-group-item list-group-item-action">Accueil</a>
        <a href="{{url('items')}}" class="list-group-item list-group-item-action">Item</a>
        <a href="{{url('marque')}}" class="list-group-item list-group-item-action">Marque</a>
        <a href="{{url('marqueProduits')}}" class="list-group-item list-group-item-action">Marque et produit</a>
    </div>
@endsection