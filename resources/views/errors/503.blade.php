{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Oups une erreur ces produite')

@section('titre', 'Servie non valide. Site en maintenance')
@section('h1Titre', 'Le site web est en maintenance, revenez dans quelque minute et cela seras régler.')
<p>@include('flash::message')</p>

@section('contenu')
    <div class="container">
        <p>Nous sommes désolé, nos développeur travail sur notre site. </p>
    </div>
@endsection