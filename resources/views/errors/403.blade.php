{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Vous n\'avez pas les droits d\'acces')

@section('titre', 'Vous n\'avez pas les droits requis')
@section('h1Titre', 'Vous n\'avez pas les droits requis')
<p>@include('flash::message')</p>

@section('contenu')
    <div class="container">
        <p>Vous n'avez pas les droits d'accès pour effectuer cette fonction. Par contre vous pouvez continuer à naviguer
            sur notre site avec grand plaisir. Voici les pages que vous avez accàs à la base.</p>
    </div>
    <div class="list-group">
        <a href="{{url('/')}}" class="list-group-item list-group-item-action">Accueil</a>
        <a href="{{url('items')}}" class="list-group-item list-group-item-action">Item</a>
        <a href="{{url('marque')}}" class="list-group-item list-group-item-action">Marque</a>
        <a href="{{url('marqueProduits')}}" class="list-group-item list-group-item-action">Marque et produit</a>
    </div>
@endsection



