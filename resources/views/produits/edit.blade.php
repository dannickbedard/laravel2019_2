@extends('layouts.app')
@section('metaDescription', 'Ajouter un produit a ce merveilleux répertoire de ski.')
@section('titre', 'Ajouter un produit')
@include('flash::message')
@section('contenu')
    <div class="container">
        <h1>Modifier un produit de ski</h1>
        <br>
        <form method="POST" action="{{ route('items.update', [$produit->id]) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <p>{{$produit->nom}}</p>
            <div class="form-group row">
                <label for="nom" class="col-form-label col-sm-2">*Nom : </label>
                <div class="col-sm-10">
                    <input type="text" maxlength="150" required class="form-control" name="nom" id="nom"
                           value="{{ old('nom', $produit->nom)}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-form-label col-sm-2">*Description : </label>
                <div class="col-sm-10">
                    <input type="text" required class="form-control" name="description" id="description"
                           value="{{ old('description', $produit->description) }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="marque" class="col-form-label col-sm-2">*Marque : </label>
                <select class="form-control col-sm-10" id="marque_id" name="marque_id">
                    <option value="">Veuillez choisir...</option>
                    @foreach($marques as $marque)
                        <option value="{{ $marque->id }}" {{ $marque->id == $produit->marques->first()->id ? 'selected' : null }}>{{ $marque->nom }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                @if($produit->imageProduits->count())
                    <img data-id="{{ $produit->imageProduits->first()->id }}" class="imageSurpprimer"
                         alt="{{$produit->description}}"
                         src="{{asset('medias/produits/' .  $produit->imageProduits->first()->image)}}">
                    <input class="hide" id="image" name="image" type="file">
                @else
                    <div class="form-group row">
                        <label for="image" class="col-form-label col-sm-2">* Photo : </label>
                        <div class="col-sm-10">
                            <input id="image" name="image" type="file">
                        </div>
                    </div>
                @endif
                <a class=""><img height="25" class="crud supprimer" width="25"
                                 src="{{asset('medias/commun/cancel-button.svg')}}"></a>
            </div>
            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <input type="submit" class="btn btn-primary" value="Enregistrer">
                </div>
            </div>
        </form>
    </div>
@endsection
@if(isset($errors) && $errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
{{--Source : https://github.com/proengsoft/laravel-jsvalidation--}}
<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

{!! JsValidator::formRequest('App\Http\Requests\ProduitRequest') !!}







