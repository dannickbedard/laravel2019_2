{{--/*
|--------------------------------------------------------------------------
| Examen
|--------------------------------------------------------------------------
*/--}}
<div class="produits">
    @if ($clients->count() > 0)
        <table class="table">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clients as $client)
                <tr>
                    <th>{{ $client->nomfamille }}</th>
                    <th>{{ $client->prenom }}</th>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>Il n'y a présentement aucun client qui possède ce statut.</p>
    @endif
</div>