<!DOCTYPE html>
<html>
<head>
    <link rel="icon"
          href="https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/20108644_1813330145646796_4997049542149889967_n.jpg?_nc_cat=109&_nc_ht=scontent-yyz1-1.xx&oh=ee6ae82096a56010e823392f90c6676c&oe=5CF7F43B"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/messtyle.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>
<body>
<div class="">
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=t8kb1vs6gweqxvgjsz0c6zi0a5mh1upu824kr6884i7md8m3"></script>
    <script>
        tinymce.init({
            selector: 'textarea.titre',
            menubar: false,
            plugins: 'paste, charmap, link, image, media, lists, code, table',

            style_formats: [
                {title: 'Important', inline: 'span', classes: 'important'},
                {title: 'Paragraphe important', block: 'p', classes: 'important'},
                {title: 'Barré', inline: 'span', classes: 'barre'},
                {title: 'Couleur 1', inline: 'span', classes: 'couleur1'},
                {title: 'Couleur 2', inline: 'span', classes: 'couleur2'},
                {title: 'Couleur 3', inline: 'span', classes: 'couleur3'}
            ],
            toolbar: 'undo redo | bold italic | bullist numlist | table tableinsertrowbefore tableinsertcolbefore tabledeleterow tabledeletecol | pastetext removeformat | code | CODE | charmap link image media | styleselect formatselect',
            content_css: [
                '/css/tinymce.css'
            ]
        });
        tinymce.init({
            selector: 'textarea.texte',
            menubar: false,
            plugins: 'paste, charmap, link, image, media, lists, code, table',

            style_formats: [
                {title: 'Important', inline: 'span', classes: 'important'},
                {title: 'Paragraphe important', block: 'p', classes: 'important'},
                {title: 'Barré', inline: 'span', classes: 'barre'},
                {title: 'Couleur 1', inline: 'span', classes: 'couleur1'},
                {title: 'Couleur 2', inline: 'span', classes: 'couleur2'},
                {title: 'Couleur 3', inline: 'span', classes: 'couleur3'}
            ],
            toolbar: 'undo redo | bold italic | bullist numlist | table tableinsertrowbefore tableinsertcolbefore tabledeleterow tabledeletecol | pastetext removeformat | code | CODE | charmap link image media | styleselect formatselect',
            content_css: [
                '/css/tinymce.css'
            ]
        });
    </script>
    <div class="container">
        <form method="post" action="{{route('page.save')}}">
            {{ csrf_field() }}
            <div class="title">
                <label for="title">Titre de l'onglet</label>
                <input class="form-control" name="title" id="title"
                       value="{{ old('title', $contenue->first()->title) }}">
            </div>
            <div class="description">
                <label for="description">Description</label>
                <input class="form-control" name="description" id="description"
                       value="{{ old('title', $contenue->first()->description) }}">
            </div>
            <div class="titre">
                <label for="titre">Titre</label>
                <textarea name="titre" class="titre" id="titre">{{$contenue->first()->titre}}</textarea>
            </div>
            <div class="texte">
                <label for="texte">Texte</label>
                <textarea class="texte" name="texte"> {{$contenue->first()->texte}}</textarea>
            </div>
            <div class="bouton">
                <span class="enregistrer btn btn-primary">Enregistrer</span>
                <a class="retour btn btn-info" href="{{ url()->previous() }}">Retour</a>

            </div>
        </form>
    </div>
    <script>
        $('.enregistrer').click(function (event) {
            event.preventDefault();

            console.log("Enregistrer");
            // formulaire dans lequel la balise qui a généré l'appel AJAX était placée
            var $formulaire = $(this).parents("form:first");
            var donneesFormulaire = $formulaire.serializeArray();
            var actionFormulaire = $formulaire.attr('action');

            // ajoute dans le POST le contenu du textarea associé à TinyMCE (ici, introduction = attribut name du textarea)
            var titre = tinyMCE.get('titre').getContent();
            var texte = tinyMCE.get('texte').getContent();
            donneesFormulaire.push(
                {name: 'titre', value: titre},
                {name: 'texte', value: texte},
                {name: 'url', value: "{{$contenue->first()->url}}"}
                );

            console.log('Donne formulaire' + donneesFormulaire);
            console.log('Action' + actionFormulaire);
            // appel AJAX
            $.ajax({
                type: "GET",
                url: actionFormulaire,
                dataType: "json",
                data: $.param(donneesFormulaire) // le $.param() est nécessaire puisqu'on a utilisé .serializeArray() plutôt que .serialize()
            }).done(function (response, textStatus, jqXHR) {
                    if (response.valide) {
                        console.log('Enregistrer avec succès');
                        console.log(response.pageTexte);
                        console.log(response.titre);
                        console.log(response.description);
                        console.log(response.texte);
                        console.log(response.title);
                        console.log(response.url);
                    }
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    console.log('fail')
                });
        });
    </script>
</div>
</body>
</html>