{{-- zone de texte seulement --}}
@extends('layouts.app')
@section('metaDescription', 'Créer votre compte ici. Avoir la possibiliter de créer une liste de favori')

@section('ajouteClient' , 'active')
@section('titre', 'Créer un compte')
<p>@include('flash::message')</p>
@section('contenu')
    <form class="supprimer" method="POST" action="{{ route('produits.destroy', [ $produit ]) }}">
        @csrf
        @method('DELETE')

        <h3>Voulez vous vraiment supprimer ce produit?</h3>
        <p>La suppression est une opération irréversible. Il n'a aucune façons de revenir en arrière.</p>

        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <input type="submit" class="btn btn-primary" value="Supprimer">
            </div>
        </div>
    </form>
@endsection
@section('js')
@endsection