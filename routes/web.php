<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| Examen route
|--------------------------------------------------------------------------
*/
Route::get('/client', [
    'as'   => 'client.show',
    'uses' => 'ClientsController@index',
]);
Route::post('/afficherClient', [
    'as'   => 'client.get',
    'uses' => 'ClientsController@getClient',
]);

Auth::routes();
/*
|--------------------------------------------------------------------------
| Accueil
|--------------------------------------------------------------------------
*/
Route::get('/', [
    'as'   => 'accueil.blade.php',
    'uses' => 'PagesController@index',
]);
/*
|--------------------------------------------------------------------------
| TinyMCE
|--------------------------------------------------------------------------
*/
Route::get('/tinymce', [
    'as'   => 'tinymce.view',
    'uses' => 'tinyController@index',
]);
Route::get('/tinymce/Save/Texte', [
    'as'  => 'tinymce.saveTexte',
    'uses' => 'tinyController@saveTexte'
]);

/*
|--------------------------------------------------------------------------
| Affichage de données
|--------------------------------------------------------------------------
*/
Route::get('/items', [
    'as'   => "items.blade.php",
    'uses' => 'ItemsController@index',
]);
Route::get('/marques', [
    'as'   => "marques.blade.php",
    'uses' => 'MarquesController@index',
]);
Route::get('/marquesProduits', [
    'as'   => "marquesProduits.blade.php",
    'uses' => 'MarquesProduitsController@index',
]);
/*
|--------------------------------------------------------------------------
| Formulaire d'ajout
|--------------------------------------------------------------------------
*/
Route::get('/produitscreation', [
    'as'   => 'produits.create',
    'uses' => 'ItemsController@create',
]);
Route::get('/marquescreation', [
    'as'   => 'marques.create',
    'uses' => 'MarquesController@create',
]);
Route::get('/clientCreation', [
    'as'   => 'client.create',
    'uses' => 'UsagersController@create',
]);
Route::get('/contact', [
    'as'   => "contact.blade.php",
    'uses' => 'CommentairesController@index',
]);
Route::get('/login', [
    'as'   => 'client.login',
    'uses' => 'UsagersController@login',
]);
Route::get('/logout', [
    'as'   => 'client.logout',
    'uses' => 'UsagersController@logout',
]);

/*
|--------------------------------------------------------------------------
| Enregistrer dans la base de données
|--------------------------------------------------------------------------
*/
Route::post('items', [
    'as'   => 'produits.store',
    'uses' => 'ItemsController@store'
])->middleware('can:create,App\Produit');
Route::post('marques', [
    'as'   => 'marques.store',
    'uses' => 'MarquesController@store'
]);
Route::post('commentaires', [
    'as'   => 'commentaires.store',
    'uses' => 'CommentairesController@store',
]);
Route::post('usagers', [
    'as'   => 'usagers.store',
    'uses' => 'UsagersController@store',
]);

/*
|--------------------------------------------------------------------------
| Authentification
|--------------------------------------------------------------------------
*/
Route::post('login', [
    'as'   => 'usager.login',
    'uses' => 'UsagersController@createLogin',
]);
/*
|--------------------------------------------------------------------------
| Modification des données
|--------------------------------------------------------------------------
*/
Route::get('items/{produit}/modification', [
    'as'   => 'items.edit',
    'uses' => 'ItemsController@edit',
])->middleware('can:edit,produit');
Route::patch('items/{produit}', [
    'as'   => 'items.update',
    'uses' => 'ItemsController@update',
])->middleware('can:edit,produit');
/*
|--------------------------------------------------------------------------
| Supprimer
|--------------------------------------------------------------------------
*/
Route::get('items/{produit}/suppression', [
    'as'   => 'items.destroy',
    'uses' => 'ItemsController@routeSupression',
]);

Route::delete('produits/{produit}', [
    'as'   => 'produits.destroy',
    'uses' => 'ItemsController@destroy',
]);
/*
|--------------------------------------------------------------------------
| Edit une page
|--------------------------------------------------------------------------
*/
Route::get('editionPage/{url}', [
    'as'   => 'page.edit',
    'uses' => 'PagesController@showEdit',
]);

Route::get('saveTexte', [
    'as'   => 'page.save',
    'uses' => 'PagesController@saveTexte',
]);






