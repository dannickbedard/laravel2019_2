<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Produit extends Model
{
    //

    protected $fillable = [
      'nom',
      'description',
    ];

    public function imageProduits(): HasMany
    {
        return $this->hasMany('App\ImageProduit');
    }

    public function marques(): BelongsToMany{
        return $this->belongsToMany('App\Marque');
    }
}
