<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ImageMarque extends Model
{
    //
    public function marque(): BelongsTo
    {

        return $this->belongsTo('App\Marque');
    }
}
