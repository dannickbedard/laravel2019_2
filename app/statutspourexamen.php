<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class statutspourexamen extends Model
{
    /*
     |--------------------------------------------------------------------------
     | Examen
     |--------------------------------------------------------------------------
     */
    public function client(): BelongsToMany{
        return $this->belongsToMany('App\clientspourexamen');
    }
}
