<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ImageProduit extends Model
{
    //
    public function produit(): BelongsTo
    {
        return $this->belongsTo('App\Produit');
    }

}
