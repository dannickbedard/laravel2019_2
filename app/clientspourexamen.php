<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientspourexamen extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Examen
    |--------------------------------------------------------------------------
    */
    public function statu(): BelongsToMany{
        return $this->belongsToMany('App\statutspourexamen');
    }
}
