<?php
/**
 * Created by PhpStorm.
 * User: danni
 * Date: 24/févr./2019
 * Time: 19:50
 */

/**

 * Fait un dump and die seulement si en mode débogage.

 * Source : christianelagace.com

 *

 * @param  mixed

 * @return void

 */

function ddd($donnees) : void

{

    if (Config::get('app.debug')) {

        dd($donnees);

    }

}