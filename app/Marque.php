<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Marque extends Model
{
    protected $fillable = [
        'nom',
        'description',
    ];

    public function imageMarques(): HasMany
    {

        return $this->hasMany('App\ImageMarque');
    }
    public function produit(): BelongsToMany{
        return $this->belongsToMany('App\Produit');
    }
}
