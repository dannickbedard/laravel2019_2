<?php
/**
 * Created by PhpStorm.
 * User: danni
 * Date: 07/févr./2019
 * Time: 19:22
 */

namespace App\Http\Controllers;

use App\Http\Requests\StoreMultipleForm;
use App\MaBibliotheque\MaClasse;
use App\Marque;
use App\Marque_Produit;
use App\Produit;
use App\ImageProduit;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Http\Requests\ProduitRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Laracasts\Flash\Flash;

include_once(app_path() . '/fonctions/debogage.php');
include_once(app_path() . '/fonctions/fonctions.php');

class ItemsController extends Controller
{
    public function accueil()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits = Produit::orderBy('nom')->get();
        return View('pages.items', compact('produits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {//regarde si le client est connecter.
            $marques = Marque::orderBy('nom')->get();
            return View('pages.createProduit', compact('marques'));
        } else {
            return back(); //S'il est connecter nous le retournons a la page pécedent.
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProduitRequest $request): RedirectResponse
    {
        $this->authorize('create', Produit::class);
        try {
            $reussi = false;
            $file = $request->file('image');   // 'image' est l'attribut name du <input type="file">
            $nomFichierOriginal = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $nomFichier = stringToSlug($nomFichierOriginal) . '-' . uniqid();
            $extension = $file->extension();

            $produit = new Produit($request->all());
            $slug = stringToSlug(uniqid()); //J'étulise le id car il est unique
            $produit->save();
            $produit->marques()->sync($request->get('marque_id'));
            $reussi = true; //Le produit a réussi a s'enregistrer

            /* Téléversement de l'image
            $file sera de type Symfony\Component\HttpFoundation\File\File
            si on n'a pas besoin de la variable qui représente le fichier après l'avoir déplacé, on peut faire le move sans retenir $file */
            $file = $file->move(public_path() . "/medias/produits/$slug", $nomFichier . '.' . $extension);

            //Enregistrer le nom de l'image dans la BD
            $imageProduit = new ImageProduit();
            $imageProduit->image = $slug . '/' . $nomFichier . '.' . $extension;
            $imageProduit->produit_id = $produit->id;
            $imageProduit->description = $request->description;
            $imageProduit->save();

            if ($reussi) {
                flash('Le produit a été enregistré avec succès !')->success();
                return back();

            } else {
                flash('Le produit a été enregistré avec succès, mais un problème a empêché le téléversement de sa photo.')->warning();
                return back();
            }
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            \Log::error("Erreur lors du téléversement du fichier. ", [$e]);
            \flash('Erreur lors du téléversement du fichier.')->error();
            return back();
        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error("Erreur lors de l'enregistrement. ", [$e]);
            \flash('Une erreur est survenue lors de l\'enregistrement.')->error();
        } catch (\Throwable $e) {
            \Log::error('Erreur inattendue : ', [$e]);
            \flash('Le produit n\'a pas enregistrer pour une raison obscure.')->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        if (Auth::check()) {//regarde si le client est connecter.
            $marques = Marque::orderBy('description')->get();
            return View('produits.edit', compact('produit', 'marques'));
        } else {
            return back(); //S'il est connecter nous le retournons a la pafe pécedent.
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProduitRequest $request, Produit $produit): RedirectResponse
    {
        $this->authorize('edit', $produit);
        try {
            $reussi = false;

            $produit->nom = $request->nom;
            $produit->description = $request->description;
            $produit->save();
            $produit->marques()->sync($request->get('marque_id'));

            $reussi = true; //Le produit a réussi a se mofifier

            if ($file = $request->file('image')) { //Vérifie s'il y a une image a ajouter
                $imagesASupprimer = json_decode($request->jsonImagesASupprimer);

                $imageProduit = ImageProduit::findOrFail($imagesASupprimer)->first();
                if (!empty($imageProduit)) {
                    \File::Delete(public_path() . "/medias/produits/$imageProduit->image");
                    $imageProduit->delete();

                } else {
                    $file = $request->file('image');   // 'image' est l'attribut name du <input type="file">

                    $nomFichierOriginal = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $nomFichier = stringToSlug($nomFichierOriginal) . '-' . uniqid();
                    $extension = $file->extension();
                    $slug = stringToSlug(uniqid()); //J'étulise le id car il est unique

                    $file = $file->move(public_path() . "/medias/produits/$slug", $nomFichier . '.' . $extension);

                    //Enregistrer le nom de l'image dans la BD
                    $imageProduit = new ImageProduit();
                    $imageProduit->image = $slug . '/' . $nomFichier . '.' . $extension;
                    $imageProduit->produit_id = $produit->id;
                    $imageProduit->description = $request->description;
                    $imageProduit->save(); //Enregistre l'image
                }
            }
            if ($reussi) {
                flash('Le produit a été enregistré avec succès !')->success();
                return back();
            } else {
                flash('Le produit a été enregistré avec succès, mais un problème a empêché le téléversement de sa photo.')->warning();
                return back();
            }
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            \Log::error("Erreur lors du téléversement du fichier. ", [$e]);
            \flash('Erreur lors du téléversement du fichier.')->error();
            return back();
        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error("Erreur lors de l'enregistrement. ", [$e]);
            \flash('Une erreur est survenue lors de l\'enregistrement.')->error();
        } catch (\Throwable $e) {
            \Log::error('Erreur inattendue : ', [$e]);
            \flash('Le produit n\'a pas enregistrer pour une raison obscure.')->error();
            return back();
        }
    }

    /**
     * @param $id Produit->id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function routeSupression($produit)
    {
        return View('supprimer.item', compact('produit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $produit = Produit::findOrFail($id); // Trouve le produit

            $this->authorize('delete', $produit);
            $imageProduit = $produit->imageProduits()->first(); //Trouve l'image relier a produit

            \File::Delete(public_path() . "/medias/produits/$imageProduit->image"); // Supprime la photo
            $produit->imageProduits()->delete();
            $produit->marques()->detach();
            $produit->delete();

            flash('Le produit a été supprimer avec succès!')->success();
            $reussi = true;
            return json_encode(compact('reussi')); //Retourne la variable réussi
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            \Log::error("Erreur lors du téléversement du fichier. ", [$e]);
            \flash('Erreur lors du téléversement du fichier.')->error();

        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error("Erreur lors de l'enregistrement. ", [$e]);
            \flash('Une erreur est survenue lors de l\'enregistrement.')->error();

        } catch (\Throwable $e) {
            \Log::error('Erreur inattendue : ', [$e]);
            \flash('Le produit n\'a pas enregistrer pour une raison obscure.')->error();

        }
    }
}