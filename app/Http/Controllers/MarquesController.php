<?php
/**
 * Created by PhpStorm.
 * User: danni
 * Date: 10/févr./2019
 * Time: 12:22
 */

namespace App\Http\Controllers;

use App\Marque;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\MarqueRequest;
use Laracasts\Flash\Flash;
use Auth;

class MarquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marques = Marque::orderBy('nom')->get();
        return View('pages.marques', compact('marques'));
    }

    public function create()
    {
        if (Auth::check()) {//regarde si le client est connecter.
            return View('pages.createMarque');
        } else {
            return back(); //S'il est connecter nous le retournons a la pafe pécedent.
        }
    }

    /**
     * Enregistre la nouvelle marque.
     * @param MarqueRequest $request Les données du formulaire
     */
    public function store(MarqueRequest $request): RedirectResponse
    {
        //todo :: Cette fonction ne fonctionne pas.
        try {
            $marques = new Marque($request->all());
            $marques->save();
            flash('La marque a été enregistré avec succès !')->success();
            return redirect()->action('MarquesController@index');
        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error("Erreur lors de l'enregistrement. ", [$e]);
            \flash('Une erreur est survenue lors de l\'enregistrement.')->error();
        } catch (\Throwable $e) {
            \Log::error('Erreur inattendue : ', [$e]);
        }
    }
}