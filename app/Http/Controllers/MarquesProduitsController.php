<?php
/**
 * Created by PhpStorm.
 * User: danni
 * Date: 20/févr./2019
 * Time: 20:15
 */

namespace App\Http\Controllers;


use App\Marque;
use App\Produit;

class MarquesProduitsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View Affiche marque produit
     */
    public function index()
    {
        $marques = Marque::orderBy('nom')->get();
        return View('pages.marqueProduit', compact('marques'));
    }

}