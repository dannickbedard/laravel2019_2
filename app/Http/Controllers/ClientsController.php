<?php

namespace App\Http\Controllers;

use App\clientspourexamen;
use App\statutspourexamen;
use Illuminate\Http\Request;
use App\Statu;
use Illuminate\Support\Facades\Auth;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *
     * Examen
     */
    /*
    |--------------------------------------------------------------------------
    | Examen
    |--------------------------------------------------------------------------
    */
    public function index()
    {
        if (Auth::check()) {//regarde si le client est connecter.
            $reussi = true;
            $status = statutspourexamen::orderBy('description')->get();
            $clients = clientspourexamen::orderBy('nomfamille')->get();
            return View('pages.clients', compact('status', 'clients'));
        } else {
            return back(); //S'il est connecter nous le retournons a la pafe pécedent.
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Examen
    |--------------------------------------------------------------------------
    */
    /**
     * @return string Retourne la liste de clients
     */
    public function getClient(): string
    {
        try {
            $valide = true;
            $contenuHTML = '';

            // retrouve l'information passée par AJAX
            $id = $_POST['statut_id'];
            $clients = clientspourexamen::where('statut_id', $id)->orderBy('nomfamille')->get();   // sera une collection vide si le select est sur "Veuillez choisir..."
            $contenuHTML = View('partials.clients', compact('clients'))->render();
            return json_encode(compact('valide', 'contenuHTML'));

        } catch (\Throwable $e) {
            \Log::error("Erreur inattendue. ", [$e]);
            $valide = false;
            return json_encode(compact('valide', 'contenuHTML'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
