<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\UsagerRequest;
use App\Usager;
use Carbon\Carbon;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use Auth;

class UsagersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) { //regarde si le client est connecter.
            return back(); //S'il est connecter nous le retournons a la pafe pécedent.
        } else {
            return View('pages.createClient');
        }
    }

    /**
     * Regarde si l'utilisateur est connecté
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|\Illuminate\View\View nous retourne a la page de connexion
     */
    public function login()
    {
        if (Auth::check()) {//regarde si le client est connecter.
            return back(); //S'il est connecter nous le retournons a la pafe pécedent.
        } else {
            return View('pages.connection');
        }
    }

    /**
     * Nous déconnecte
     * @return RedirectResponse Nous retourne a la page précédente
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
            return back();
        } else {
            return back();
        }
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function createLogin(LoginRequest $request)
    {
        $courriel = $request->courriel;

        $usager = Usager::where('courriel', $courriel)->first();
        $actif = $usager->actif;
        $motdepasse = $request->motdepasse;

        if ($actif) { // Vérifie si l'utilisateur est actif
            $reussi = Auth::attempt(['courriel' => $courriel, 'password' => $motdepasse,]);

        } else {
            $reussi = false;
        }


        if ($reussi) {
            /*Aucun message flash car l'usager vas savoir qu'il est connecter par le simple fait que le bouton connection
            seras changer pas le bouton de déconnection. */
            return redirect()->action('PagesController@index');

        } else {
            flash('Vérifier votre courriel ou votre mot de passe.')->error();
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsagerRequest $request): RedirectResponse
    {
        try {
            $ajouterUsager = new Usager($request->all());
            $ajouterUsager->motdepasse = bcrypt($request->motdepase);
            $ajouterUsager->actif = 1;
            $ajouterUsager->save();

            flash('L\'enregistrement ces fait a merveille')->success();
            return redirect()->route('client.login'); //Passer la route par un redirect

        } catch (\Throwable $e) {
            \Log::error('Erreur inattendue : ', [$e]);
            \flash('Vous n\'avez pas plus vous enregistrer pour une raison obscure.')->error();
            return back(); //redirect()->action('PagesController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
