<?php

namespace App\Http\Controllers;

use App\Commentaire;

use App\Page;
use Illuminate\Http\Request;
include_once(app_path() . '/fonctions/fonctions.php');

class PagesController extends Controller
{
    public function accueil()
    {

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commentaires = Commentaire::where('url', \Route::getCurrentRoute()->uri())->orderBy('dateAjout', 'desc')->get();

        // Vas chercher l'url de la page pour afficher le texte de la page.
        $textes = Page::where('url', \Route::getCurrentRoute()->uri())->orderBy('created_at', 'desc')->get();

        return View('pages.index', compact('commentaires' , 'textes'));//Pages
        // nom du dossier dans views. index = index.blade.php
    }

    /**
     * Nous apporte sur la page pour éditer une page.
     *
     * @param $urlRecu L'url de la page précédente.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEdit($urlRecu)
    {
        $url = urlOuSlashSiAccueil($urlRecu);

        $contenue = Page::where('url', $url)->orderBy('created_at', 'desc')->first();
        $this->authorize('editPage', $contenue);

        return view('edit.pageEdit', compact('contenue'));
    }

    /**
     *
     * @return false|string Retourne les informations des nouvelles pages.
     */
    public function saveTexte()
    {
        $titre = $_GET['titre'];
        $title = $_GET['title'];
        $description = $_GET['description'];
        $texte = $_GET['texte'];
        $url = $_GET['url'];

        $pageTexte = Page::where('url', $url)->orderBy('created_at', 'desc')->first();

        $this->authorize('editPage', $pageTexte); // Les droits d'éditer une page.

        $pageTexte->title = $title;
        $pageTexte->titre = $titre;
        $pageTexte->description = $description;
        $pageTexte->texte = $texte;

        $pageTexte->save();

        //Je fait cettre actions pour pouvoir avoir accès après mon appelle ajax
        $titre = $pageTexte->titre;
        $title = $pageTexte->title;
        $description = $pageTexte->description;
        $texte = $pageTexte->texte;


        $valide = true;
        return json_encode(compact('valide', 'pageTexte', 'titre', 'texte', 'title', 'url', 'description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
