<?php

namespace App\Http\Controllers;

use App\Commentaire;
use App\Http\Requests\CommentairRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use App\Page;

class CommentairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$commentaires = Commentaire::where('url', Route::getCurrentRoute()->uri())->orderBy('dateAjout', 'desc')->get();
        $commentaires = Commentaire::orderBy('dateAjout', 'desc')->get();
        // Vas chercher l'url de la page pour afficher le texte de la page.
        $textes = Page::where('url', \Route::getCurrentRoute()->uri())->orderBy('created_at', 'desc')->get();
        return View('pages.contact', compact('commentaires', 'textes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentairRequest $request)
    {
        try {

            $ajoutCommentaire = new Commentaire($request->all());
            $ajoutCommentaire->dateAjout = Carbon::now();
            $url = url()->previous() == url('/') ? '/' : preg_replace('@' . url('/') . '/@', '', url()->previous());

            if ($url === '')//Car si je ne fait pas ca l'url vas être rien. Je veut qu'il écrive a la place /
            {
                $url = '/';
            }
            $ajoutCommentaire->url = $url;
            $ajoutCommentaire->save();
            if ($url === "contact") {
                flash('Le commentaire a été enregistré avec succès !')->success();
                return back(); //redirect()->action('CommentairesController@index');
            } else {
                flash('Le commentaire a été enregistré avec succès !')->success();
                return back(); // redirect()->action('PagesController@index');
            }
        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error("Erreur lors de l'enregistrement. ", [$e]);
            \flash('Une erreur est survenue lors de l\'enregistrement.')->error();
        } catch (\Throwable $e) {
            \Log::error('Erreur inattendue : ', [$e]);
            \flash('Le commentaires n\'a pas enregistrer pour une raison obscure.');
            return back(); //redirect()->action('PagesController@index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
