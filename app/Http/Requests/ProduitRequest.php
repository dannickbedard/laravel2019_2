<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProduitRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nom'         => 'required|min:2|max:150',
            'description' => 'required',
            'marque_id'   => 'required|exists:marques,id',
            'image'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
        return $rules;
    }

    /**
     * Affiche les messages d'supprimer si une utilisateur oublie d'entrer un champ du formulaire.
     *
     * @return array Message d'erreur
     */
    public function messages()
    {
        return [
            'nom'         => 'Le produit doit avoir a un nom.',
            'description' => 'Le produit doit avoir une description.',
            'marque_id'   => 'Vous devez indiquer le produit appartient à quel marque.',
            'image'       => 'Vous devez fournir l\'image du produit que vous voulez ajouter.',
        ];
    }
}
