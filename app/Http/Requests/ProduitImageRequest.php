<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProduitImageRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'image'       => 'min:3',
            'description' => 'required|max:255',
            'produit_id'  => 'required',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'nom'         => 'Le produit doit avoir a un nom.',
            'description' => 'Le produit doit avoir une description.',
        ];
    }
}
