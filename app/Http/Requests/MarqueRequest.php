<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarqueRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nom'         => 'required|min:3|max:255',
            'description' => 'required|min:3',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'nom'         => 'La marque doit avoir a un nom.',
            'description' => 'La marque doit avoir une description.',
        ];
    }
}
