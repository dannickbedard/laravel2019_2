<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentairRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nom'         => 'max:150',
            'prenom'      => 'max:150',
            'courriel'    => 'required|email|max:255',
            'commentaire' => 'required',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'courriel'    => 'Entrer votre  courriel',
            'commentaire' => 'Entrer votre commentaire',
        ];
    }
}
