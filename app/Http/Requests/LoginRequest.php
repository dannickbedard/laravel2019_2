<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'courriel'                => 'email|max:255',
            'motdepasse'              => 'min:8',
        ];
        return $rules;
    }

    /**
     * @return array Retourne les messages d'erreur
     */
    public function message()
    {
        return [
            'courriel'               => 'Vous devez entrer votre courriels pour vous connecter',
            'motdepasse'             => 'Vous devez entrer votre mot de passe pour vous connecter.',
        ];
    }
}
