<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class UsagerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nom'                     => 'max:150',
            'premon'                  => 'max:150',
            'courriel'                => 'email|max:255|unique:usagers',
            'motdepasse'              => ['confirmed|min:8', Rule::notIn(['1234567890', 'qwerty1234', 'password1234'])],
            'motdepasse_confirmation' => 'min:8'

        ];
        return $rules;
    }

    /**
     * @return array Retourne les messages d'erreur
     */
    public function message()
    {
        return [
            'nom'                    => 'Vous devez entrer votre nom.',
            'prenom'                 => 'Vous devex entrer votre prénom.',
            'courriel'               => 'Vous devez entrer votre courriels',
            'motdepasse'             => 'Vous devez entrer votre mot de passe.',
            'confirmationmotdepasse' => 'Vous devez confirmer votre mot de passe.'
        ];
    }
}
