<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;


use Illuminate\Database\Eloquent\Model;

class Usager extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'nom',
        'prenom',
        'courriel',

    ];

    protected $hidden = [
        'motdepasse',
        'remember_token',
    ];
    protected $visible = [
        'nom',
        'prenom',
        'courriel'
    ];

    /**
     * Retrouve le mot de passe de l'usager.
     * Nécessaire pour l'authentification puisque Laravel a besoin d'un champ qui s'appelle password.
     * Voir : http://stackoverflow.com/questions/26073309/how-to-change-custom-password-field-name-for-laravel-4-and-laravel-5-user-auth
     *
     * @return string
     */

    public function getAuthPassword() {

        return $this->motdepasse;

    }
}
