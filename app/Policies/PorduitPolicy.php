<?php

namespace App\Policies;

use App\Produit;
use App\Usager;
use Illuminate\Auth\Access\HandlesAuthorization;

class PorduitPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(Usager $usager, Produit $produit): bool
    {
        $autorise = false;
      if ($usager->administrateur){
          $autorise = true;
      }
        return $autorise;
    }

    public function delete(Usager $usager, Produit $produit): bool
    {
        $autorise = false;
        if ($usager->administrateur){
            $autorise = true;
        }
        return $autorise;
    }

    /**
     * Determine si produit peut être créé par l'usager.
     *
     * @param \App\Usager $usager
     * @return bool
     */
    public function create(Usager $usager): bool
    {
        $autorise = false;
        if ($usager->administrateur){ // Si l'utilisateur a le niveau 1 de l'administration
            $autorise = true;
        }
        return $autorise;
    }
}
