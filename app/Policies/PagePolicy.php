<?php

namespace App\Policies;

use App\Usager;
use App\Page;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function editPage(Usager $usager, Page $page): bool
    {
        $autorise = false;
        if ($usager->administrateur){
            $autorise = true;
        }
        return $autorise;
    }

    /**
     * Determine si produit peut être créé par l'usager.
     *
     * @param \App\Usager $usager
     * @return bool
     */
    public function showPage(Usager $usager): bool
    {
        $autorise = false;
        if ($usager->administrateur){ // Si l'utilisateur a le niveau 1 de l'administration
            $autorise = true;
        }
        return $autorise;
    }
}
