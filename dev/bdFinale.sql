-- MySQL dump 10.13  Distrib 5.6.37, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: laravel2019_bedarddannick
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientspourexamens`
--

DROP TABLE IF EXISTS `clientspourexamens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientspourexamens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomfamille` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statut_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clientspourexamens_statut_id_foreign` (`statut_id`),
  CONSTRAINT `clientspourexamens_statut_id_foreign` FOREIGN KEY (`statut_id`) REFERENCES `statutspourexamens` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientspourexamens`
--

LOCK TABLES `clientspourexamens` WRITE;
/*!40000 ALTER TABLE `clientspourexamens` DISABLE KEYS */;
INSERT INTO `clientspourexamens` VALUES (1,'Lacasse','Toto',1,NULL,NULL),(2,'Gagnon','Annie',4,NULL,NULL),(3,'Gamache','Martin',4,NULL,NULL),(4,'Cantin','Kariane',5,NULL,NULL),(5,'Courtois','Yves',1,NULL,NULL),(6,'Courtois','Loïc',1,NULL,NULL),(7,'Alie','Martin',4,NULL,NULL),(8,'Hamel','Jessie',5,NULL,NULL),(9,'Beaudoin','Pierre-Luc',5,NULL,NULL),(10,'Cantin','Julie',5,NULL,NULL);
/*!40000 ALTER TABLE `clientspourexamens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courriel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateAjout` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaires`
--

LOCK TABLES `commentaires` WRITE;
/*!40000 ALTER TABLE `commentaires` DISABLE KEYS */;
INSERT INTO `commentaires` VALUES (1,'Bedard','Dannick','/','dannickbedard@gmail.com','Voici les commentaires','2018-01-11','2018-01-11 10:16:32',NULL),(2,'Lacasse','Toto','/','toto@gmail.com','votre sute est une vraie mine d\'or.','2019-02-28','2018-01-11 10:16:32',NULL),(3,'Gagnon','Annie','contact','anniegagnon@gmail.com','Bonjours, j\'aurais besion de plus d\'information sur vos produits, Est-ce qu\'un représentant pourrait me contacter? Merci! ','2019-03-10','2018-01-11 10:16:32',NULL),(4,'Courtois','Luc','/','luc.courtois@hotmail.com','Beau site! ','2019-03-12','2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `commentaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_marques`
--

DROP TABLE IF EXISTS `image_marques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_marques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marque_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image_marques_marque_id_foreign` (`marque_id`),
  CONSTRAINT `image_marques_marque_id_foreign` FOREIGN KEY (`marque_id`) REFERENCES `marques` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_marques`
--

LOCK TABLES `image_marques` WRITE;
/*!40000 ALTER TABLE `image_marques` DISABLE KEYS */;
INSERT INTO `image_marques` VALUES (1,'Armada ski','armadaLogo.png',1,'2018-01-11 10:16:32',NULL),(2,'Jski','JskiLogo.png',2,'2018-01-11 10:16:32',NULL),(3,'K2 ski','k2Logo.jpeg',3,'2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `image_marques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_produits`
--

DROP TABLE IF EXISTS `image_produits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_produits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `produit_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image_produits_produit_id_foreign` (`produit_id`),
  CONSTRAINT `image_produits_produit_id_foreign` FOREIGN KEY (`produit_id`) REFERENCES `produits` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_produits`
--

LOCK TABLES `image_produits` WRITE;
/*!40000 ALTER TABLE `image_produits` DISABLE KEYS */;
INSERT INTO `image_produits` VALUES (1,'Jski cascade et ces très le fun','JSkiCascade.jpg',1,'2018-01-11 10:16:32',NULL),(2,'Ski Armada fait pour la poudreuse de l\'ouest','armadaNorWalk2015.jpg',2,'2018-01-11 10:16:32',NULL),(3,'Ski de park','k2Ski244_1.jpg',3,'2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `image_produits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marque_produit`
--

DROP TABLE IF EXISTS `marque_produit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marque_produit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marque_id` int(10) unsigned NOT NULL,
  `produit_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marque_produit_marque_id_foreign` (`marque_id`),
  KEY `marque_produit_produit_id_foreign` (`produit_id`),
  CONSTRAINT `marque_produit_marque_id_foreign` FOREIGN KEY (`marque_id`) REFERENCES `marques` (`id`),
  CONSTRAINT `marque_produit_produit_id_foreign` FOREIGN KEY (`produit_id`) REFERENCES `produits` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marque_produit`
--

LOCK TABLES `marque_produit` WRITE;
/*!40000 ALTER TABLE `marque_produit` DISABLE KEYS */;
INSERT INTO `marque_produit` VALUES (1,1,2,'2018-01-11 10:16:32',NULL),(2,2,1,'2018-01-11 10:16:32',NULL),(3,3,3,'2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `marque_produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marques`
--

DROP TABLE IF EXISTS `marques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marques`
--

LOCK TABLES `marques` WRITE;
/*!40000 ALTER TABLE `marques` DISABLE KEYS */;
INSERT INTO `marques` VALUES (1,'Armada','Ski colorer et marque de linge fait pour les skieurs','2018-01-11 10:16:32',NULL),(2,'Jski','Ski fait a la main et édition limiter. Createur de Line ski et full tilt boot','2018-01-11 10:16:32',NULL),(3,'K2','K2 Skis has been seeking fun through innovation since 1962. Our all-mountain philosophy and technological advancements continue to lead the ski industry.','2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `marques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2019_02_06_184600_create_pages_table',1),(3,'2019_02_06_184800_create_marques_table',1),(4,'2019_02_06_184851_create_imagesmarque_table',1),(5,'2019_02_06_184900_create_produits_table',1),(6,'2019_02_06_184957_create_marquesproduits_table',1),(7,'2019_02_06_185056_create_imageproduit_table',1),(8,'2019_03_13_162004_create_commentaires_table',1),(9,'2019_03_27_184910_create_usagers_table',1),(10,'2019_05_14_193102_create_statutspourexamen_table',1),(11,'2019_05_14_193121_create_clientspourexamen_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_url_unique` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Page d\'accueil du site laravel 2019','Vous chercher un répertoire des skis que une marque a dejat fait. Vous chercher l\'information comportent un ski en particulier? Vous le trouverez ici. Nous avons le plus gros répertoire de ski de les marques les plus connues.','Catalogue de Ski','Catalogue de Ski','/','2018-01-11 10:16:32',NULL),(2,'page de contact','Bonjours, mon est Dannick Béedard. Je suis étudiant au CEGEP de Victoriaville depuis dejat 3ans. Mon\n                cheminement a été prelonger,\n                car j\'ai échouer un cours de math. Je fait ce projet étudiant dans le cadre de notre cours de\n                développement\n                web 2. Le développement web est une chose que j\'aime bien faire. Il me reste encore beaucoup de chose a\n                apprendre\n                pour devenir un vrai développeur web.','page de contact','page de contact','contact','2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produits`
--

DROP TABLE IF EXISTS `produits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produits`
--

LOCK TABLES `produits` WRITE;
/*!40000 ALTER TABLE `produits` DISABLE KEYS */;
INSERT INTO `produits` VALUES (1,'Jski Cascade','Ski fait sur mesure','2018-01-11 10:16:32',NULL),(2,'Armada NorWalk 2015','Ski de poudreuse','2018-01-11 10:16:32',NULL),(3,'K2 244','Ski de park','2018-01-11 10:16:32',NULL);
/*!40000 ALTER TABLE `produits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statutspourexamens`
--

DROP TABLE IF EXISTS `statutspourexamens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statutspourexamens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordre` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statutspourexamens`
--

LOCK TABLES `statutspourexamens` WRITE;
/*!40000 ALTER TABLE `statutspourexamens` DISABLE KEYS */;
INSERT INTO `statutspourexamens` VALUES (1,'vip',50,NULL,NULL),(2,'bronze',20,NULL,NULL),(3,'argent',30,NULL,NULL),(4,'or',40,NULL,NULL),(5,'régulier',10,NULL,NULL);
/*!40000 ALTER TABLE `statutspourexamens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usagers`
--

DROP TABLE IF EXISTS `usagers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usagers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courriel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `motdepasse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `administrateur` tinyint(1) DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usagers_courriel_unique` (`courriel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usagers`
--

LOCK TABLES `usagers` WRITE;
/*!40000 ALTER TABLE `usagers` DISABLE KEYS */;
INSERT INTO `usagers` VALUES (1,'Bedard','Dannick','dannickbedard@gmail.com','2019-05-24 17:32:42','$2y$10$YAhE4XH4N5rFGUABULxzFO1ofN/8XMFKhHAINx9F.ToJ/cHR4ddBC','',1,1,'r7rVrgsAlpvUCTdV9Yl4q2W0J3AMKRtklE8PORDE30kVTWbEm0Whz0N6AO8d',NULL,NULL),(2,'Bedard','Dannick','dannickbedard06@gmail.com','2018-01-11 10:16:32','$2y$10$b4EtqfACtrSGpuawffP.zuJv5E5p/60oOmScpSRC93KyCyZG2nAMq','',1,1,NULL,NULL,NULL),(3,'Bedard','Dannick','dannickbedard07@gmail.com','2019-05-24 17:33:13','$2y$10$v/lvHvWxgOyUWlXVaDbnJe2RALed/Yr5jdCWLG8WjaSMrYmEXaATO',NULL,NULL,1,NULL,'2019-05-24 17:17:33','2019-05-24 17:17:33');
/*!40000 ALTER TABLE `usagers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-24 16:01:35
