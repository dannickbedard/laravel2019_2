<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarquesproduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marque_produit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marque_id')->unsigned();
            $table->integer('produit_id')->unsigned();
            $table->timestamps();
            $table->foreign('marque_id')->references('id')->on('marques');
            $table->foreign('produit_id')->references('id')->on('produits');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marques_produits');
    }
}
