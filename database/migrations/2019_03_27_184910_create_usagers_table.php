<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usagers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom', 150);
            $table->string('prenom',150);
            $table->string('courriel', 255)->unique();
            $table->timestamp('email_verified_at');
            $table->string('motdepasse');
            $table->string('imageUrl')->nullable();
            $table->boolean('administrateur')->nullable();
            $table->boolean('actif')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usagers');
    }
}
