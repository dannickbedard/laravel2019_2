<?php

use Illuminate\Database\Seeder;

class CommentairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('commentaires')->insert([
            [
                'nom' => 'Bedard',
                'prenom' => 'Dannick',
                'courriel' => 'dannickbedard@gmail.com',
                'url' => '/',
                'commentaire' => 'Voici les commentaires',
                'dateAjout' => '2018-01-11 10:16:32',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'nom' => 'Lacasse',
                'prenom' => 'Toto',
                'courriel' => 'toto@gmail.com',
                'url' => '/',
                'commentaire' => 'votre sute est une vraie mine d\'or.',
                'dateAjout' => '2019-02-28',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'nom' => 'Gagnon',
                'prenom' => 'Annie',
                'courriel' => 'anniegagnon@gmail.com',
                'url' => 'contact',
                'commentaire' => 'Bonjours, j\'aurais besion de plus d\'information sur vos produits, Est-ce qu\'un représentant pourrait me contacter? Merci! ',
                'dateAjout' => '2019-03-10',
                'created_at' => '2018-01-11 10:16:32',
            ],[
                'nom' => 'Courtois',
                'prenom' => 'Luc',
                'courriel' => 'luc.courtois@hotmail.com',
                'url' => '/',
                'commentaire' => 'Beau site! ',
                'dateAjout' => '2019-03-12',
                'created_at' => '2018-01-11 10:16:32',
            ],
        ]);
    }
}
