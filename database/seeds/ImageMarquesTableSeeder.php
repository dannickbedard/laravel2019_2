<?php

use Illuminate\Database\Seeder;

class ImageMarquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('image_marques')->insert([
            [
                'description' => 'Armada ski',
                'image' => 'armadaLogo.png',
                'marque_id' => 1,
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'description' => 'Jski',
                'image' => 'JskiLogo.png',
                'marque_id' => 2,
                'created_at' => '2018-01-11 10:16:32',
            ],[
                'description' => 'K2 ski',
                'image' => 'k2Logo.jpeg',
                'marque_id' => 3,
                'created_at' => '2018-01-11 10:16:32',
            ],
        ]);
    }
}
