<?php

use Illuminate\Database\Seeder;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produits')->insert([
            [
                'nom' => 'Jski Cascade',
                'description' => 'Ski fait sur mesure',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'nom' => 'Armada NorWalk 2015',
                'description' => 'Ski de poudreuse',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'nom' => 'K2 244',
                'description' => 'Ski de park',
                'created_at' => '2018-01-11 10:16:32',
            ],
        ]);
    }
}
