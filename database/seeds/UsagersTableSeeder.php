<?php

use Illuminate\Database\Seeder;

class UsagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usagers')->insert([
            [
                'nom' => 'Bedard',
                'prenom' => 'Dannick',
                'courriel' => 'admin@gmail.com',
                'email_verified_at' => '2018-01-11 10:16:32',
                'motdepasse' => '$2y$10$YAhE4XH4N5rFGUABULxzFO1ofN/8XMFKhHAINx9F.ToJ/cHR4ddBC',
                'imageUrl' => '',
                'administrateur' => 1,
                'actif' => 1,
            ], [
                'nom' => 'Bedard',
                'prenom' => 'Dannick',
                'courriel' => 'dannickbedard06@gmail.com',
                'email_verified_at' => '2018-01-11 10:16:32',
                'motdepasse' => bcrypt('qwerty1234'),
                'imageUrl' => '',
                'administrateur' => 0,
                'actif' => 1,
            ],
        ]);
    }
}
