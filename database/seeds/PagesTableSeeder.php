<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'description' => 'Page d\'accueil du site laravel 2019',
                'url'         => '/',
                'titre'       => 'Catalogue de Ski',
                'title'       => 'Catalogue de Ski',
                'texte'       => 'Vous chercher un répertoire des skis que une marque a dejat fait. Vous chercher l\'information comportent un ski en particulier? Vous le trouverez ici. Nous avons le plus gros répertoire de ski de les marques les plus connues.',

                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'description' => 'page de contact',
                'url'         => 'contact',
                'titre'       => 'page de contact',
                'title'       => 'page de contact',
                'texte'       => 'Bonjours, mon est Dannick Béedard. Je suis étudiant au CEGEP de Victoriaville depuis dejat 3ans. Mon
                cheminement a été prelonger,
                car j\'ai échouer un cours de math. Je fait ce projet étudiant dans le cadre de notre cours de
                développement
                web 2. Le développement web est une chose que j\'aime bien faire. Il me reste encore beaucoup de chose a
                apprendre
                pour devenir un vrai développeur web.',
                'created_at'  => '2018-01-11 10:16:32',
            ],

        ]);
    }
}
