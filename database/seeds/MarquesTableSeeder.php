<?php

use Illuminate\Database\Seeder;

class MarquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marques')->insert([
            [
                'nom' => 'Armada',
                'description' => 'Ski colorer et marque de linge fait pour les skieurs',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'nom' => 'Jski',
                'description' => 'Ski fait a la main et édition limiter. Createur de Line ski et full tilt boot',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'nom' => 'K2',
                'description' => 'K2 Skis has been seeking fun through innovation since 1962. Our all-mountain philosophy and technological advancements continue to lead the ski industry.',
                'created_at' => '2018-01-11 10:16:32',
            ],

        ]);
    }
}
