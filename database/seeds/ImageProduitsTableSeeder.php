<?php

use Illuminate\Database\Seeder;

class ImageProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('image_produits')->insert([
            [
                'description' => 'Jski cascade et ces très le fun',
                'image' => 'JSkiCascade.jpg',
                'produit_id' => 1,
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'description' => 'Ski Armada fait pour la poudreuse de l\'ouest',
                'image' => 'armadaNorWalk2015.jpg',
                'produit_id' => 2,
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'description' => 'Ski de park',
                'image' => 'k2Ski244_1.jpg',
                'produit_id' => 3,
                'created_at' => '2018-01-11 10:16:32',
            ],
        ]);
    }
}
