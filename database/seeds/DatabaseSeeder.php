<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Importation de tout les fichiers seed
     *
     * @return void
     */
    public function run()
    {
        $this->call(TablesTableSeeder::class);
        $this->call(MarquesTableSeeder::class);
        $this->call(ProduitsTableSeeder::class);
        $this->call(ImageProduitsTableSeeder::class);
        $this->call(ImageMarquesTableSeeder::class);
        $this->call(Marques_ProduitsTableSeeder::class);
        $this->call(CommentairesTableSeeder::class);
        $this->call(UsagersTableSeeder::class);
        $this->call(StatutspourexamenTableSeeder::class);
        $this->call(ClientspourexamenTableSeeder::class);
        $this->call(PagesTableSeeder::class);
    }
}
