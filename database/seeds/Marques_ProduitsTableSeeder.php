<?php

use Illuminate\Database\Seeder;

class Marques_ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marque_produit')->insert([
            [
                'marque_id' => '1',
                'produit_id' => '2',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'marque_id' => '2',
                'produit_id' => '1',
                'created_at' => '2018-01-11 10:16:32',
            ],
            [
                'marque_id' => '3',
                'produit_id' => '3',
                'created_at' => '2018-01-11 10:16:32',
            ],
        ]);
    }
}
